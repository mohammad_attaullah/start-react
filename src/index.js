import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import registerServiceWorker from './registerServiceWorker';


function FormatName(props) {
    return <h1>Hello there, {props.firstName + ' ' + props.lastName}</h1>;
}
class Clock extends React.Component {
    constructor(props) {
        super(props);
        this.state = { date: new Date() };
    }
    componentDidMount() {
        this.timerID = setInterval(
            () => this.tick(),
            1000
        );
    }
    componentWillUnmount() {
        clearInterval(this.timerID);
    }
    tick() {
        this.setState({
            date: new Date()
        });
    }
    render() {
        return (
            <div>
                <h1>Hello, world!</h1>
                <h2>It is {formateTime(this.state.date)}.</h2>
            </div>
        );
    }
}

ReactDOM.render(<Clock />, document.getElementById('timer'));


const user = {
    firstName: 'Harper',
    lastName: 'Perez'
};
function formateTime(date) {
    return date.toLocaleTimeString();
}
function formatDate(date) {
    return date.toLocaleDateString();
}
function Avatar(props) {
    return (
        <img className="Avatar"
            src={props.user.avatarUrl}
            alt={props.user.name}
        />
    );
}

function UserInfo(props) {
    return (
        <div className="UserInfo">
            <Avatar user={props.user} />
            <div className="UserInfo-name">
                {props.user.name}
            </div>
        </div>
    );
}

function Comment(props) {
    return (
        <div className="Comment">
            <UserInfo user={props.author} />
            <div className="Comment-text">
                {props.text}
            </div>
            <div className="Comment-date">
                {formatDate(props.date)}
            </div>
        </div>
    );
}

const comment = {
    date: new Date(),
    text: 'I hope you enjoy learning React!',
    author: {
        name: 'Hello Kitty',
        avatarUrl: 'http://placekitten.com/g/64/64'
    }
};
function ShowAvatar() {
    return <Comment
        date={comment.date}
        text={comment.text}
        author={comment.author} />
}

const element = <FormatName firstName={user.firstName} lastName={user.lastName} />;

ReactDOM.render(<App />, document.getElementById('root'));
ReactDOM.render(element, document.getElementById('custom'));
ReactDOM.render(<ShowAvatar />, document.getElementById('avatar'));
registerServiceWorker();
